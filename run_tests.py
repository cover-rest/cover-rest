from unittest import main

from restApi.parser.test.xml_parse import TestCobertura, TestJacoco
from restApi.database.test.database_interface import TestProject, TestCommit, TestCoverage
from restApi.swagger_server.test.test_project_controller import TestProjectController
from restApi.swagger_server.test.test_commit_controller import TestCommitController
from restApi.swagger_server.test.test_coverage_controller import TestCoverageController


if __name__ == "__main__":
    main()
