# Cover-Rest

## Goals of Cover-Rest

The primary goals of the [prototypefund project](https://prototypefund.de/en/project/cover-rest-find-features-avoid-stress/) of Cover-Rest were to 'rethink' [Code Coverage](docs/code-coverage.md) and test the new identified use cases.

We focused on three new aspects in our project:

1. Creating a [Code Cognita](docs/use-case-1__code-cognita.md) - finding features in source code!
2. Lightspeed [Fast Feature Tests](docs/use-case-2__fast-feature-test.md) for Commits using a Code Cognita!
3. [Modularization](docs/use-case-3__code-coverage-modularization.md) of the Coverage software stack using OpenAPI!

## Usage Example of Cover-Rest

From the beginning we had two usage examples in mind, both related to the ODF (OpenDocument Format) file format, where features of the file format are represented

1. In the ODF software the viewer/writer of the file format and
2. In the ODF file format itself, specified by the OASIS ODF Standard

This was especially interesting to us to be able to find features in the software by creating an ODF test document and removing the desired feature in a copy from it.

In the first scenario the loading of each documents would create a test coverage. Saving and inserting the feature by the UI or API of the software will folllow.

We have choosen two different ODF software to test Cover-Rest.

1. An [adoption for Java using ODFDOM](./docs/coverrest-usage_java_odfdom.md), a library supporting [ODF](https://github.com/oasis-tcs/odf-tc) from the [ODF Toolkit](https://github.com/tdf/odftoolkit).
2. An [adoption for C++ using LibreOffice](./docs/coverrest-usage_cpp_libreoffice.md). [LibreOffice is an office Suite](https://www.libreoffice.org/) supporting [ODF](https://github.com/oasis-tcs/odf-tc).

Other adoptions with other software context are surely possible and welcome!

## Follow-up of Cover-Rest

A follow-up project for us (or for others) could lead into various interesting directions:

1. Enhancement of Coverage Data Serialization. Isn't the Coverage Data just a subset of data from a performance test? This approach would be far more CPU/Time efficient!
There are some [open libraries for performance annotation (OpenACC)](https://en.wikipedia.org/wiki/OpenACC).
2. Wouldn't the gathering of Coverage/Performance Data using the Static approach more efficient? Dependent on the changed Code Flow Graph only a subtree has to be re-tested?
3. There should certainly be some performance pattern extractable by Machine Learning (if not already done).

The [latest information are also available online](https://cover-rest.gitlab.io/)!

## Installation

clone repo: `git clone --recurse-submodules -j8 https://gitlab.com/cover-rest/cover-rest.git`

init submodules: `git submodule update --init --recursive`

install python packages: `pip install -r requirements.txt`

## Run

start db : `sudo systemctl start mongod.service`
server: `python3 -m restApi.swagger_server`
interface: `python3 process.py -h`

## Testing

run test coverage: `coverage run --source . -m pytest run_tests.py`

