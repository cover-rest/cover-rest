import argparse
import logging
import sys
import requests
import re
import os.path

import yaml

from restApi.parser.xml_parse import XmlParse


class CoverRestInterface:
    pass


def __validate_url(_url: str, route: str, correction: bool = True):
    if _url[-1] == "/":
        _url = _url[:-1]
    _url_split_slash = _url.split("/")
    with open(os.path.join(os.path.dirname(__file__), "restApi/swagger_server/swagger/swagger.yaml")) as file:
        spec = yaml.full_load(file.read(310))
    _api_url_split_slash = ["Cover-Rest", "Interface-API"]
    version = spec.get("info").get("version")
    _url_query_split = _url_split_slash[-1].split("?")
    if not all(e in _url_split_slash for e in _api_url_split_slash):
        if correction:
            _old_url = _url
            _url = '/'.join(_url_split_slash + _api_url_split_slash + [version, route])
            logging.warning(f"api route missing trying to fix url: {_old_url}, result: {_url}")
        else:
            logging.warning(f"api route missing, url should be containing: "
                            f"{'/'.join(_api_url_split_slash + [version, route])}")
    elif version not in _url_split_slash:
        _old_url = _url
        if correction:
            if _url_split_slash[-1].find(".") != -1:
                _url_split_slash[-1] = version
            elif _url_split_slash[-2].find(".") != -1:
                _url_split_slash[-2] = version
            else:
                logging.warning(f"version mismatch, current version is: {version}")
                return _url
            _url = "/".join(_url_split_slash)
            logging.warning(f"version mismatch trying to fix url: {_old_url}, result: {_url}")
        else:
            logging.warning(f"version mismatch, current version is: {version}")
    elif route not in _url_query_split:
        _old_url = _url
        if _url_split_slash[-1].find("?") != -1:
            if correction:
                _url_split_slash[-1] = f"{route}?{_url_query_split[-1]}"
                logging.warning(f"route mismatch trying to fix url: {_old_url}, result: {_url}")
            else:
                logging.warning(f"route mismatch, correct one is {route}")
        else:
            if correction:
                _url_split_slash = _url_split_slash + [route]
                _url = '/'.join(_url_split_slash)
                logging.warning(f"route missing trying to fix url: {_old_url}, result: {_url}")
            else:
                logging.warning(f"route missing, route is {route}")
    return _url


def __check_for_query(_url: str, queries: list) -> list:
    if _url.find("?") == -1:
        return []
    _queries_found = []
    _url_query = _url.split("?")[-1]
    _url_queries = re.split('=&,|%', _url_query)
    for _query in _url_queries:
        if _query in queries:
            _queries_found.append(_query)
    return queries


def project_add(*, _project_title: str, _project_url: str, api_key: str = None,
                _project_description: str, _api_url: str, correction: bool = True) -> str:
    if not api_key:
        api_key = os.getenv("MODIFY_API_KEY")
    if not api_key:
        raise Exception("Missing API key, set env: MODIFY_API_KEY or as use parameter")
    _api_url = __validate_url(_api_url, "project", correction)
    _res = requests.post(_api_url, headers={'MODIFY-API-KEY': api_key},  # TODO impl api key
                         json={"title": _project_title, "url": _project_url, "description": _project_description})
    if _res.status_code == 201:
        _project_id = _res.json().get("id")
        if _project_id is not None:
            return _project_id
        else:
            raise Exception(_res.status_code, _res.text, "no id in response")
    else:
        raise Exception(_res.status_code, _res.text)


def project_get(*, _api_url: str, _project_id: str = None, _project_url: str = None,
                _project_title: str = None, _all: bool = False, correction: bool = True) -> dict:
    _api_url = __validate_url(_api_url, "project", correction)
    _queries_found = __check_for_query(_api_url, ["projectID", "projectTitle", "projectUrl", "getAll"])
    if len(_queries_found) > 0:  # TODO maybe check if queries are correct
        logging.info("using url query parameter")
    elif _all:
        _api_url += "?getAll=true"
    elif _project_id:
        _api_url += f"?projectID={_project_id}"
    elif _project_title:
        _api_url += f"?projectTitle={_project_title}"
    elif _project_url:
        _api_url += f"?projectUrl={_project_url}"
    else:
        raise Exception("missing input parameters")
    _res = requests.get(_api_url)
    if _res.status_code == 200:
        return _res.json()
    else:
        raise Exception(_res.status_code, _res.text)


def commit_add(*, _commit_hash: str, _message: str, _branch: str, _api_url: str, correction: bool = True,
               _project_id: str = None, _project_url: str = None, _project_title: str = None,
               api_key: str = None) -> str:
    if not api_key:
        api_key = os.getenv("MODIFY_API_KEY")
    if not api_key:
        raise Exception("Missing API key, set env: MODIFY_API_KEY or as use parameter")
    _api_url = __validate_url(_api_url, "commit", correction)
    _queries_found = __check_for_query(_api_url, ["projectID", "projectTitle", "projectUrl"])
    if len(_queries_found) > 0:  # TODO maybe check if queries are correct
        logging.info("using url query parameter")
    elif _project_id:
        _api_url += f"?projectID={_project_id}"
    elif _project_title:
        _api_url += f"?projectTitle={_project_title}"
    elif _project_url:
        _api_url += f"?projectUrl={_project_url}"
    else:
        raise Exception("missing input parameters")
    _res = requests.post(_api_url, headers={'MODIFY-API-KEY': api_key},  # TODO impl api key
                         json={"commit_hash": _commit_hash, "message": _message, "branch": _branch})
    if _res.status_code == 201:
        _commit_id = _res.json().get("id")
        if _commit_id is not None:
            return _commit_id
        else:
            raise Exception(_res.status_code, _res.text, "no id in response")
    else:
        raise Exception(_res.status_code, _res.text)


def commit_get(*, _api_url: str, _commit_id: str = None, _commit_hash: str = None, correction: bool = True,
               _project_id: str = None, _project_url: str = None, _project_title: str = None,
               _all: bool = False) -> dict:
    _api_url = __validate_url(_api_url, "commit", correction)
    _queries_found = __check_for_query(_api_url, ["commitID", "commitHash", "projectID", "projectTitle", "projectUrl"])
    if len(_queries_found) > 0:  # TODO maybe check if queries are correct
        logging.info("using url query parameter")
    elif _all and _project_id:
        _api_url += f"?getAll=true&projectID={_project_id}"
    elif _all and _project_title:
        _api_url += f"?getAll=true&projectTitle={_project_title}"
    elif _all and _project_url:
        _api_url += f"?getAll=true&projectUrl={_project_url}"
    elif _commit_id:
        _api_url += f"?commitID={_commit_id}"
    elif _commit_hash and _project_id:
        _api_url += f"?commitHash={_commit_hash}&projectID={_project_id}"
    elif _commit_hash and _project_title:
        _api_url += f"?commitHash={_commit_hash}&projectTitle={_project_title}"
    elif _commit_hash and _project_url:
        _api_url += f"?commitHash={_commit_hash}&projectUrl={_project_url}"
    else:
        raise Exception("missing input parameters")
    _res = requests.get(_api_url)
    if _res.status_code == 200:
        return _res.json()
    else:
        raise Exception(_res.status_code, _res.text)


def coverage_add(*, _filename: str, _job_reference: str, _coverage_description: str, _api_url: str,
                 _commit_id: str = None, _commit_hash: str = None, correction: bool = True, api_key: str = None,
                 _project_id: str = None, _project_url: str = None, _project_title: str = None, reduce: bool = True
                 ) -> str:
    if not api_key:
        api_key = os.getenv("MODIFY_API_KEY")
    if not api_key:
        raise Exception("Missing API key, set env: MODIFY_API_KEY or as use parameter")
    _api_url = __validate_url(_api_url, "coverage", correction)
    _queries_found = __check_for_query(_api_url, ["commitID", "commitHash", "projectID", "projectTitle", "projectUrl"])
    if len(_queries_found) > 0:  # TODO maybe check if queries are correct
        logging.info("using url query parameter")
    elif _commit_id:
        _api_url += f"?commitID={_commit_id}"
    elif _commit_hash and _project_id:
        _api_url += f"?commitHash={_commit_hash}&projectID={_project_id}"
    elif _commit_hash and _project_title:
        _api_url += f"?commitHash={_commit_hash}&projectTitle={_project_title}"
    elif _commit_hash and _project_url:
        _api_url += f"?commitHash={_commit_hash}&projectUrl={_project_url}"
    else:
        raise Exception("missing input parameters")
    _res = XmlParse.from_path(_filename, reduce).to_api_db(_api_url, api_key, _job_reference, _coverage_description)
    if _res.status_code == 201:
        _commit_id = _res.json().get("id")
        if _commit_id is not None:
            return _commit_id
        else:
            raise Exception(_res.status_code, _res.text, "no id in response")
    else:
        raise Exception(_res.status_code, _res.text)


def coverage_get(*, _output_path: str, _api_url: str, correction: bool = True,
                 _coverage_id: str = None, _job_reference: str = None,
                 _commit_id: str = None, _commit_hash: str = None,
                 _project_id: str = None, _project_url: str = None, _project_title: str = None,
                 _force_filename: bool = False, _all: bool = False) -> str:
    _api_url = __validate_url(_api_url, "coverage", correction)
    _queries_found = __check_for_query(_api_url, ["coverageID", "jobReference",
                                                  "commitID", "commitHash",
                                                  "projectID", "projectTitle", "projectUrl"])
    if len(_queries_found) > 0:  # TODO maybe check if queries are correct
        logging.info("using url query parameter")
    elif _all and _commit_id:
        _api_url += f"?getAll=true&commitID={_commit_id}"
    elif _all and _commit_hash and _project_id:
        _api_url += f"?getAll=true&commitHash={_commit_hash}&projectID={_project_id}"
    elif _all and _commit_hash and _project_title:
        _api_url += f"?getAll=true&commitHash={_commit_hash}&projectTitle={_project_title}"
    elif _all and _commit_hash and _project_url:
        _api_url += f"?getAll=true&commitHash={_commit_hash}&projectUrl={_project_url}"
    elif _coverage_id:
        _api_url += f"?coverageID={_coverage_id}"
    elif _job_reference and _commit_id:
        _api_url += f"?jobReference={_job_reference}&commitID={_commit_id}"
    elif _job_reference and _commit_hash and _project_id:
        _api_url += f"?jobReference={_job_reference}&commitHash={_commit_hash}&projectID={_project_id}"
    elif _job_reference and _commit_hash and _project_title:
        _api_url += f"?jobReference={_job_reference}&commitHash={_commit_hash}&projectTitle={_project_title}"
    elif _job_reference and _commit_hash and _project_url:
        _api_url += f"?jobReference={_job_reference}&commitHash={_commit_hash}&projectUrl={_project_url}"
    else:
        raise Exception("missing input parameters")
    XmlParse.from_api_db(_output_path, _api_url, _force_filename)
    if not os.path.isdir(_output_path):
        _output_path = "/".join(_output_path.split("/")[:-1])
    return _output_path


if __name__ == "__main__":
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    main_parser = argparse.ArgumentParser()
    sub_parsers = main_parser.add_subparsers()

    subcommand_project = sub_parsers.add_parser('project', help='project -h')
    sub_parser_project = subcommand_project.add_subparsers()
    subcommand_project_add = sub_parser_project.add_parser(
        "add", help='project add -h',
        description='you need to supply all positional arguments to add an new project '
                    'and api-key if MODIFY_API_KEY not set as env variable')
    subcommand_project_add.set_defaults(command="project_add")
    subcommand_project_add.add_argument("project_title", help='project title')
    subcommand_project_add.add_argument("project_url", help='project url')
    subcommand_project_add.add_argument("project_description", help='project description')
    subcommand_project_add.add_argument("api_url", help='API url')
    subcommand_project_add.add_argument("--api-key", help="sets api key, can use env variable MODIFY_API_KEY instead")
    subcommand_project_add.add_argument("-k", "--no-correction", help="disable api_url correction", default=False)
    subcommand_project_get = sub_parser_project.add_parser(
        "get", help='project get -h',
        description='you need to supply supply all positional arguments and '
                    'PROJECT_ID or PROJECT_URL or PROJECT_TITLE options to get project or ALL to get all projects, '
                    'these parameters can also be as query in the api_url, but in Camel case')
    subcommand_project_get.set_defaults(command="project_get")
    subcommand_project_get.add_argument("api_url", help='API url')
    subcommand_project_get.add_argument("-p", "--project-id", help='project id')
    subcommand_project_get.add_argument("-u", "--project-url", help='project url')
    subcommand_project_get.add_argument("-t", "--project-title", help='project title')
    subcommand_project_get.add_argument("-a", "--all", action="store_true", default=False,
                                        help='returns all projects')
    subcommand_project_get.add_argument("-k", "--no-correction", help="disable api_url correction", default=False)

    subcommand_commit = sub_parsers.add_parser('commit', help='commit -h')
    sub_parser_commit = subcommand_commit.add_subparsers()
    subcommand_commit_add = sub_parser_commit.add_parser(
        "add", help='commit add -h',
        description='you need to supply all positional arguments and api-key if MODIFY_API_KEY not set as env variable '
                    'and PROJECT_ID or PROJECT_URL or PROJECT_TITLE options to create new commit, '
                    'these parameters can also be as query in the api_url, but in Camel case')
    subcommand_commit_add.set_defaults(command="commit_add")
    subcommand_commit_add.add_argument("commit_hash", help='commit hash')
    subcommand_commit_add.add_argument("message", help='commit message')
    subcommand_commit_add.add_argument("branch", help='commit branch')
    subcommand_commit_add.add_argument("api_url", help='API url')
    subcommand_commit_add.add_argument("--api-key", help="sets api key, can use env variable MODIFY_API_KEY instead")
    subcommand_commit_add.add_argument("-p", "--project-id", help='project id')
    subcommand_commit_add.add_argument("-u", "--project-url", help='project url')
    subcommand_commit_add.add_argument("-t", "--project-title", help='project title')
    subcommand_commit_add.add_argument("-k", "--no-correction", help="disable api_url correction", default=False)
    subcommand_commit_get = sub_parser_commit.add_parser(
        "get", help='commit get -h',
        description='you need to supply supply all positional arguments and '
                    'COMMIT_ID or COMMIT_HASH and PROJECT_ID or '
                    'COMMIT_HASH and PROJECT_URL or COMMIT_HASH and PROJECT_TITLE '
                    'or use ALL to return all commits in project, '
                    'these parameters can also be as query in the api_url, but in Camel case')
    subcommand_commit_get.set_defaults(command="commit_get")
    subcommand_commit_get.add_argument("api_url", help='API url')
    subcommand_commit_get.add_argument("-i", "--commit-id", help='commit id')
    subcommand_commit_get.add_argument("-c", "--commit-hash", help='commit hash')
    subcommand_commit_get.add_argument("-p", "--project-id", help='project id')
    subcommand_commit_get.add_argument("-u", "--project-url", help='project url')
    subcommand_commit_get.add_argument("-t", "--project-title", help='project title')
    subcommand_commit_get.add_argument("-a", "--all", action="store_true", default=False,
                                       help='returns all commits in project')
    subcommand_commit_get.add_argument("-k", "--no-correction", help="disable api_url correction", default=False)

    subcommand_coverage = sub_parsers.add_parser('coverage', help='coverage -h')
    sub_parser_coverage = subcommand_coverage.add_subparsers()
    subcommand_coverage_add = sub_parser_coverage.add_parser(
        "add", help='coverage add -h',
        description='you need to supply all positional arguments and api-key if MODIFY_API_KEY not set as env variable '
                    'and COMMIT_ID or COMMIT_HASH and PROJECT_ID or '
                    'COMMIT_HASH and PROJECT_URL or COMMIT_HASH and PROJECT_TITLE, '
                    'these parameters can also be as query in the api_url, but in Camel case')
    subcommand_coverage_add.set_defaults(command="coverage_add")
    subcommand_coverage_add.add_argument("filename", help='filename or path to coverage file which should be added')
    subcommand_coverage_add.add_argument("job_reference", help='coverage job reference')
    subcommand_coverage_add.add_argument("coverage_description", help='coverage description')
    subcommand_coverage_add.add_argument("api_url", help='API url')
    subcommand_coverage_add.add_argument("--api-key", help="sets api key, can use env variable MODIFY_API_KEY instead")
    subcommand_coverage_add.add_argument("-i", "--commit-id", help='commit id')
    subcommand_coverage_add.add_argument("-c", "--commit-hash", help='commit hash')
    subcommand_coverage_add.add_argument("-p", "--project-id", help='project id')
    subcommand_coverage_add.add_argument("-u", "--project-url", help='project url')
    subcommand_coverage_add.add_argument("-t", "--project-title", help='project title')
    subcommand_coverage_add.add_argument("-k", "--no-correction", help="disable api_url correction", default=False)
    subcommand_coverage_add.add_argument("-r", "--no-reduce", help="disable the xml file reduction, "
                                                                   "meaning it wont remove lines with hits='0'"
                                         , default=False)
    subcommand_coverage_get = sub_parser_coverage.add_parser(
        "get", help='coverage get -h',
        description='you need to supply all positional arguments and '
                    'COVERAGE_ID or JOB_REFERENCE and COMMIT_ID or '
                    'JOB_REFERENCE and COMMIT_HASH and PROJECT_ID or '
                    'JOB_REFERENCE and COMMIT_HASH and PROJECT_URL or '
                    'JOB_REFERENCE and COMMIT_HASH and PROJECT_TITLE or '
                    'these parameters can also be as query in the api_url, but in Camel case')
    subcommand_coverage_get.set_defaults(command="coverage_get")
    subcommand_coverage_get.add_argument("output_path", help='coverage output path, or filename, '
                                                             'if a filename is given and -f is mot used'
                                                             'the cover-rest naming convention fill be applied')
    subcommand_coverage_get.add_argument("api_url", help='API url')
    subcommand_coverage_get.add_argument("-v", "--coverage-id", help='coverage id')
    subcommand_coverage_get.add_argument("-r", "--job-reference", help='coverage job reference')
    subcommand_coverage_get.add_argument("-i", "--commit-id", help='commit id')
    subcommand_coverage_get.add_argument("-c", "--commit-hash", help='commit hash')
    subcommand_coverage_get.add_argument("-p", "--project-id", help='project id')
    subcommand_coverage_get.add_argument("-u", "--project-url", help='project url')
    subcommand_coverage_get.add_argument("-t", "--project-title", help='project title')
    subcommand_coverage_get.add_argument("-f", "--force-filename", action="store_true", default=False,
                                         help='forces the filename without the cover-rest naming convention')
    subcommand_coverage_get.add_argument("-a", "--all", action="store_true", default=False,
                                         help='returns all coverage in commit, into specified file path'
                                              'if a filename is given it will take the file path')
    subcommand_coverage_get.add_argument("-k", "--no-correction", help="disable api_url correction", default=False)

    args = main_parser.parse_args()
    try:
        args.command
    except AttributeError:
        main_parser.print_help()
        sys.exit()
    if args.command == "project_add":
        project_id = project_add(_project_title=args.project_title, _project_url=args.project_url,
                                 _project_description=args.project_description, _api_url=args.api_url,
                                 correction=not args.no_correction, api_key=args.api_key)
        print(f"project_id: {project_id}")
    elif args.command == "project_get":
        project = project_get(_api_url=args.api_url, _project_id=args.project_id, _project_url=args.project_url,
                              _project_title=args.project_title, _all=args.all, correction=not args.no_correction)
        print(f"project: {project}")
    elif args.command == "commit_add":
        commit_id = commit_add(_commit_hash=args.commit_hash, _message=args.message, _branch=args.branch,
                               _api_url=args.api_url, _project_id=args.project_id, _project_url=args.project_url,
                               _project_title=args.project_title, correction=not args.no_correction,
                               api_key=args.api_key)
        print(f"commit_id: {commit_id}")
    elif args.command == "commit_get":
        commit = commit_get(_api_url=args.api_url, _commit_id=args.commit_id, _commit_hash=args.commit_hash,
                            _project_id=args.project_id, _project_url=args.project_url,
                            _project_title=args.project_title, _all=args.all, correction=not args.no_correction)
        print(f"commit: {commit}")
    elif args.command == "coverage_add":
        coverage_id = coverage_add(_filename=args.filename, _job_reference=args.job_reference,
                                   _coverage_description=args.coverage_description, _api_url=args.api_url,
                                   _commit_id=args.commit_id, _commit_hash=args.commit_hash,
                                   _project_id=args.project_id, _project_url=args.project_url,
                                   _project_title=args.project_title, correction=not args.no_correction,
                                   api_key=args.api_key, reduce=not args.no_reduce)
        print(f"coverage_id: {coverage_id}")
    elif args.command == "coverage_get":
        coverage_file_path = coverage_get(_output_path=args.output_path, _api_url=args.api_url,
                                          _job_reference=args.job_reference, _coverage_id=args.coverage_id,
                                          _commit_id=args.commit_id, _commit_hash=args.commit_hash,
                                          _project_id=args.project_id, _project_url=args.project_url,
                                          _project_title=args.project_title, correction=not args.no_correction,
                                          _force_filename=args.force_filename, _all=args.all)
        print(f"coverage_file_path: {coverage_file_path}")
    else:
        pass
