import os.path
from multiprocessing import Pool, Manager, cpu_count
import time


def strip_lines(data_ref, data_slice):
    combined_lines = ""
    try:
        for line in data_ref[data_slice]:
            found = False
            remove = False
            new_line = ""
            for char in line:
                if char in ["\n", "\r", "\t"]:
                    continue
                if char == " " and found:
                    if remove:
                        remove = False
                        new_line = new_line[:-1]
                    continue
                elif char == " ":
                    found = True
                    remove = True
                    new_line += char
                else:
                    found = False
                    remove = False
                    new_line += char
            combined_lines += new_line
    except IndexError:
        pass
    return combined_lines


def preprocess(file_in, *, file_out=None, task_per_thread=22):
    start = time.perf_counter()
    with Manager() as manager, Pool() as pool:
        with open(file_in) as input_file:
            shared_data = manager.list(input_file.readlines())

        process_count = cpu_count() * task_per_thread
        chunk_size = int(len(shared_data) / process_count + 1)
        process_list = []
        for thread_index in range(process_count):
            start_sector = thread_index * chunk_size
            process_list.append(
                pool.apply_async(strip_lines, (shared_data, slice(start_sector, start_sector + chunk_size),))
            )

        result = ""
        for index, process in enumerate(process_list):
            new_line = process.get()
            result += new_line

        if not file_out:
            if result[:400].lower().find("cobertura") != -1:
                coverage_type = "cobertura"
            elif result[:400].lower().find("jacoco") != -1:
                coverage_type = "jacoco"
            else:
                if result.find("counter") != -1:
                    coverage_type = "jacoco"
                else:
                    coverage_type = "cobertura"
            basename = os.path.basename(file_in)
            file_out = os.path.join(
                file_in.replace(basename, ""),  # filepath
                f"{coverage_type}_{basename.replace('.xml', '')}_stripped.cov.xml"
            )
            # TODO IDEA _stripped for files still containing hits="0" and _minified when those are removed

        with open(file_out, "w+") as output_file:
            output_file.write(result)

        return task_per_thread, chunk_size, len(process_list), time.perf_counter() - start


task_per_thread_, chunk_size_, len_process_list_, time_taken_ = preprocess("coverage_c.xml", task_per_thread=22)
print(f"task_per_thread_: {task_per_thread_}, chunk_size_: {chunk_size_}, "
      f"len_process_list_: {len_process_list_}, time_taken_: {time_taken_}")
