python-dotenv==0.21.0
lxml==4.9.1
multipledispatch==0.6.0
requests==2.27.1
pymongo
connexion==2.14.0
flask==2.0.3
swagger-ui-bundle==0.0.9
six==1.16.0
git+https://github.com/nickfrev/mongoengine.git